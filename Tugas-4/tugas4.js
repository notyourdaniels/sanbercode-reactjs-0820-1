//Soal 1
console.log("LOOPING PERTAMA");
var loop1 = 2;
while (loop1 <= 20) {
  console.log(loop1 + " - I love coding");
  //Pemberian if agar pada saat dia menyentuh 20, loop1 tidak +2
  if (loop1 != 20) {
    loop1 += 2;
  } else {
    break;
  }
}

console.log("LOOPING KEDUA");
while (loop1 >= 2) {
  console.log(loop1 + " - I will become a frontend developer");
  loop1 -= 2;
}

//Soal 2
for (var count2 = 1; count2 <= 20; count2++) {
  if (count2 % 2 == 0) {
    console.log(count2 + " - Berkualitas");
  } else {
    if (count2 % 3 == 0) {
      console.log(count2 + " - I Love Coding");
    } else {
      console.log(count2 + " - Santai");
    }
  }
}

//Soal 3
var hitungPagar = 0;
for (var count3 = 0; count3 < 7; count3++) {
  var pagarVertikal = 0;
  hitungPagar++;
  while (pagarVertikal < hitungPagar) {
    process.stdout.write("#");
    pagarVertikal++;
  }
  console.log();
}

//Soal 4
var kalimat = "saya sangat senang belajar javascript";
console.log(kalimat.split(" "));

//Soal 5
var daftarBuah = [
  "2. Apel",
  "5. Jeruk",
  "3. Anggur",
  "4. Semangka",
  "1. Mangga",
];
console.log(daftarBuah.sort());
