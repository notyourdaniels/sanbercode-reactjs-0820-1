//Soal 1
function halo() {
  return "Halo Sanbers!";
}

console.log(halo()); // "Halo Sanbers!"

//Soal 2
function kalikan(a, b) {
  return a * b;
}

var num1 = 12;
var num2 = 4;

var hasilKali = kalikan(num1, num2);
console.log(hasilKali); // 48

//Soal 3
function introduce(name, age, address, hobby) {
  return `Nama saya ${name}, umur saya ${age} tahun, alamat saya di ${address}, dan saya punya hobby yaitu ${hobby}!`;
}

var name = "John";
var age = 30;
var address = "jalan belum jadi";
var hobby = "Gaming";

var perkenalan = introduce(name, age, address, hobby);
console.log(perkenalan); // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di jalan belum jadi, dan saya punya hobby yaitu Gaming!

//Soal 4
var arrayDaftarPeserta = ["Asep", "laki-laki", "baca buku", 1992];
var objectDaftarPeserta = {
  nama: arrayDaftarPeserta[0],
  jenisKelamin: arrayDaftarPeserta[1],
  hobi: arrayDaftarPeserta[2],
  tahunLahir: arrayDaftarPeserta[3],
};
console.log(objectDaftarPeserta);

//Soal 5
var arrayOfObj = [
  {
    nama: "strawberry",
    warna: "merah",
    adaBijinya: "tidak",
    harga: 9000,
  },
  {
    nama: "jeruk",
    warna: "oranye",
    adaBijinya: "ada",
    harga: 8000,
  },
  {
    nama: "Semangka",
    warna: "Hijau & Merah",
    adaBijinya: "ada",
    harga: 10000,
  },
  {
    nama: "Pisang",
    warna: "Kuning",
    adaBijinya: "tidak",
    harga: 5000,
  },
];

console.log(arrayOfObj[0]);

//Soal 6
var dataFilm = [];
function tambahFilm(dataFilm) {
  dataFilm.nama = "Iron-Man 3";
  dataFilm.durasi = "120 Menit";
  dataFilm.genre = "Sci-fi, Superhero";
  dataFilm.tahun = "2014";
  return dataFilm;
}
console.log(tambahFilm(dataFilm));
