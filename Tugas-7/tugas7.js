//Nomor 1 - Release 0
class Animal {
  constructor(name) {
    this._name = name;
    this._legs = 4;
    this._coldblooded = false;
  }
  get name() {
    return this._name;
  }
  get legs() {
    return this._legs;
  }
  get cold_blooded() {
    return this._coldblooded;
  }
}

var sheep = new Animal("shaun");

console.log(sheep.name); // "shaun"
console.log(sheep.legs); // 4
console.log(sheep.cold_blooded); // false

//Nomor 1 - Release 1
class Ape extends Animal {
  constructor(name) {
    super(name);
    this._yell = "Auooo";
  }
  yell() {
    return console.log(this._yell);
  }
}

class Frog extends Animal {
  constructor(name) {
    super(name);
    this._jump = "hop hop";
  }
  jump() {
    return console.log(this._jump);
  }
}

var sungokong = new Ape("kera sakti");
sungokong.yell(); // "Auooo"

var kodok = new Frog("buduk");
kodok.jump(); // "hop hop"

//Nomor 2
class Clock {
  constructor(template) {
    this._template = template.template;
    this._timer = 0;
  }
  render() {
    this.date = new Date();

    this.hours = this.date.getHours();
    if (this.hours < 10) this.hours = "0" + this.hours;

    this.mins = this.date.getMinutes();
    if (this.mins < 10) this.mins = "0" + this.mins;

    this.secs = this.date.getSeconds();
    if (this.secs < 10) this.secs = "0" + this.secs;

    this.output = this._template
      .replace("h", this.hours)
      .replace("m", this.mins)
      .replace("s", this.secs);
    return console.log(this.output);
  }
  stop() {
    clearInterval(this._timer);
  }
  start() {
    this.render();
    this._timer = setInterval(this.render.bind(this), 1000);
  }
}

var clock = new Clock({ template: "h:m:s" });
clock.start();
