//Soal 1
let luasLingkaran = (d) => {
  return 3.14 * (d / 2) * (d / 2);
};

let kelilingLingkaran = (d) => {
  return 3.14 * d;
};

console.log(luasLingkaran(5));
console.log(kelilingLingkaran(30));

//Soal 2
const kalimat1 = "saya";
const kalimat2 = "adalah";
const kalimat3 = "seorang";
const kalimat4 = "frontend";
const kalimat5 = "developer";
let kalimat = `${kalimat1} ${kalimat2} ${kalimat3} ${kalimat4} ${kalimat5}`;
console.log(kalimat);

//Soal 3
const newFunction = (firstName, lastName) => {
  return {
    firstName,
    lastName,
    fullName: () => {
      console.log(firstName + " " + lastName);
      return;
    },
  };
};

//Driver Code
newFunction("William", "Imoh").fullName();

//Soal 4
const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!",
};
const { firstName, lastName, destination, occupation, spell } = newObject;
console.log(firstName, lastName, destination, occupation);

//Soal 5
const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];
const combined = [...west, ...east];

//Driver Code
console.log(combined);
